# V0.1943

## About:

My Voron v0.1943.
### List of mods:
- Kirigami Bed
- LGX Lite Extruder
- LEDs for Chamber
- Umbilical Coard
- Chambertemperature Sensor

## Sources

The config.cfg is based on [Voron 0.1 - SKR-mini-E3-v2.0](https://github.com/VoronDesign/Voron-0/blob/Voron0.1/Firmware/skr-mini-E3-v2.0.cfg)

As base for the macros.cfg I used [Andrew Ellis v2.247 backup](https://github.com/AndrewEllis93/v2.247_backup_klipper_config)
