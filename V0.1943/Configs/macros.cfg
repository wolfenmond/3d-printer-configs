#####################################################################
#	Print Start
#####################################################################

[gcode_macro PRINT_START]
gcode:
    # Parameters
    {% set bed = params.BED|int %}
    {% set hotend = params.HOTEND|int %}
    {% set chamber = params.CHAMBER|default(0)|int %}
    RESETSPEEDS                                                                         ; reset speed, accel etc to configured values
    M104 S140                                                                           ; set hotend to no-ooze temp
    M140 S{bed}                                                                         ; set bed temp to target temp
    G28                                                                                 ; home all axis
    G90                                                                                 ; absolute positioning
    LEDs_on_Red                                                                         ; set LEDs to red to indicate heating

    # Heatsoak the bed for printing

    {% if printer["temperature_sensor chamber"].temperature < chamber %}                ; - if chamber is not at temp yet:
        HEATSOAK T={bed} MOVE=1                                                         ;   heatsoak macro + park in center
        M190 S{bed}                                                                     ;   wait for bed final temp
        TEMPERATURE_WAIT SENSOR="temperature_sensor chamber" MINIMUM={chamber}          ;   wait for chamber final temp
    {% else %}                                                                          ; - if chamber is already at temp:
        {% if printer.heater_bed.temperature < (bed-2) %}                               ; -- but bed is not fully heated (within 2C):
            HEATSOAK T={bed} MOVE=1                                                     ;       heatsoak and park
            M190 S{bed}                                                                 ;       wait for bed final temp
        {% else %}                                                                      ; -- and bed is already heated:
            HEATSOAK T={bed} MOVE=0                                                     ;       "heatsoak" without parking (only still calling this because it does some other things like turn off exahaust fan)
        {% endif %}
    {% endif %}

    M106 S0                                                                             ; turn off part cooling fan (from heatsoak)
    G28 Z                                                                               ; final z homing
    M109 S{hotend}                                                                      ; set & wait for hotend temp
    G0 Z20                                                                              ; lift z up
    G92 E0                                                                              ; reset extruder positioning

    # PrimeLine
    G1 X0 Y-1 F10000                                                                    ; move outside of print area
    G1 Z0.3 F250                                                                        ; position z
    G1 X50 E10 F800                                                                     ; prime 1
    G1 X75 E10 F800                                                                     ; prime 2, heavy overextruded
    G1 X77 F1000                                                                        ; 2mm wipe without extruding
    G92 E0                                                                              ; reset extruder positioning
    G1 Z2 F250                                                                          ; lift z

    LEDs_on_White                                                                       ; LEDs to white

#####################################################################
#	Print End
#####################################################################

[gcode_macro PRINT_END]
gcode:
    M400                                                                                ; wait for buffer to clear
    G92 E0                                                                              ; zero the extruder
    G1 E-10.0 F300                                                                      ; retract filament
    G91                                                                                 ; relative positioning
    LEDs_on_Red                                                                         ; set LEDs to red to indicate successful print

    #   Get Boundaries
    {% set max_x = printer.configfile.config["stepper_x"]["position_max"]|float %}
    {% set max_y = printer.configfile.config["stepper_y"]["position_max"]|float %}
    {% set max_z = printer.configfile.config["stepper_z"]["position_max"]|float %}

    #   Check end position to determine safe direction to move
    {% if printer.toolhead.position.x < (max_x - 20) %}
        {% set x_safe = 20.0 %}
    {% else %}
        {% set x_safe = -20.0 %};
    {% endif %}

    {% if printer.toolhead.position.y < (max_y - 20) %}
        {% set y_safe = 20.0 %}
    {% else %}
        {% set y_safe = -20.0 %}
    {% endif %}

    {% if printer.toolhead.position.z < (max_z - 2) %}
        {% set z_safe = 2.0 %}
    {% else %}
        {% set z_safe = max_z - printer.toolhead.position.z %}
    {% endif %}

    G0 Z{z_safe} F200                                                                   ; move nozzle up
    G0 X{x_safe} Y{y_safe} F2500                                                        ; move nozzle to remove stringing
    TURN_OFF_HEATERS
    M107                                                                                ; turn off fan
    G90                                                                                 ; absolute positioning
    G0 X60 Y100 F2500                                                                   ; park nozzle at rear
    M84                                                                                 ; turn off motors


#####################################################################
#	Heatsoak
#####################################################################

[gcode_macro HEATSOAK]
gcode:
    # Parameters
    {% set t = params.T|default(110)|int %}
    {% set move = params.MOVE|default(1)|int %}

    SAVE_GCODE_STATE NAME=HEATSOAK
    UPDATE_DELAYED_GCODE ID=DELAYED_OFF DURATION=0  ; cancel off timer (if there is one)
    M140 S{t}                                       ; heat bed
    {% if t >= 100 %}
        M104 S180                                   ; set hotend to no-ooze temp
        M106 S205                                   ; turn on part fan (80%)
    {% else %}
        M106 S0                                     ; turn part fan off
    {% endif %}
    {% if move == 1 %}
        G90
        G0 Z{printer.toolhead.axis_maximum.z/2} F500
        G0 X{printer.toolhead.axis_maximum.x/2} Y{printer.toolhead.axis_maximum.y/2} F5000
    {% endif %}
    RESTORE_GCODE_STATE NAME=HEATSOAK

#####################################################################
#	Print Control
#####################################################################

[gcode_macro CANCEL_PRINT]
description: Cancel the actual running print
rename_existing: CANCEL_PRINT_BASE
gcode:
    TURN_OFF_HEATERS
    CANCEL_PRINT_BASE

[gcode_macro PAUSE]
description: Pause the actual running print
rename_existing: PAUSE_BASE
# change this if you need more or less extrusion
variable_extrude: 1.0
gcode:
    ##### read E from pause macro #####
    {% set E = printer["gcode_macro PAUSE"].extrude|float %}
    ##### set park positon for x and y #####
    # default is your max posion from your printer.cfg
    {% set x_park = printer.toolhead.axis_maximum.x|float - 5.0 %}
    {% set y_park = printer.toolhead.axis_maximum.y|float - 5.0 %}
    ##### calculate save lift position #####
    {% set max_z = printer.toolhead.axis_maximum.z|float %}
    {% set act_z = printer.toolhead.position.z|float %}
    {% if act_z < (max_z - 2.0) %}
        {% set z_safe = 2.0 %}
    {% else %}
        {% set z_safe = max_z - act_z %}
    {% endif %}
    ##### end of definitions #####
    PAUSE_BASE
    G91
    {% if printer.extruder.can_extrude|lower == 'true' %}
      G1 E-{E} F2100
    {% else %}
      {action_respond_info("Extruder not hot enough")}
    {% endif %}
    {% if "xyz" in printer.toolhead.homed_axes %}
      G1 Z{z_safe} F900
      G90
      G1 X{x_park} Y{y_park} F6000
    {% else %}
      {action_respond_info("Printer not homed")}
    {% endif %}

[gcode_macro RESUME]
description: Resume the actual running print
rename_existing: RESUME_BASE
gcode:
    ##### read E from pause macro #####
    {% set E = printer["gcode_macro PAUSE"].extrude|float %}
    #### get VELOCITY parameter if specified ####
    {% if 'VELOCITY' in params|upper %}
      {% set get_params = ('VELOCITY=' + params.VELOCITY)  %}
    {%else %}
      {% set get_params = "" %}
    {% endif %}
    ##### end of definitions #####
    {% if printer.extruder.can_extrude|lower == 'true' %}
      G91
      G1 E{E} F2100
    {% else %}
      {action_respond_info("Extruder not hot enough")}
    {% endif %}
    RESUME_BASE {get_params}

#####################################################################
#	Filament Control
#####################################################################

[gcode_macro LOAD_FILAMENT]
gcode:
    SAVE_GCODE_STATE NAME=LOADFILAMENT
    M83                            ; set extruder to relative
    G1 E30 F300                    ; load
    G1 E15 F150                    ; prime nozzle with filament
    M82                            ; set extruder to absolute
    RESTORE_GCODE_STATE NAME=LOADFILAMENT

[gcode_macro UNLOAD_FILAMENT]
gcode:
    SAVE_GCODE_STATE NAME=UNLOADFILAMENT
    M83                            ; set extruder to relative
    G1 E10 F300                    ; extrude a little to soften tip
    G1 E-80 F1800                  ; retract filament completely
    RESTORE_GCODE_STATE NAME=UNLOADFILAMENT

[gcode_macro HOT_UNLOAD]
gcode:
    # Parameters
    {% set t = params.T|default(245)|int %}

    M104 S{t}
    PARKFRONT
    M109 S{t}
    UNLOAD_FILAMENT

[gcode_macro HOT_LOAD]
gcode:
    # Parameters
    {% set t = params.T|default(245)|int %}

    M104 S{t}
    PARKFRONT
    M109 S{t}
    LOAD_FILAMENT

#####################################################################
#	LED / RGB Control
#####################################################################

[gcode_macro LEDs_on_White]
gcode:
    SET_LED LED=case_lights RED=0.3 GREEN=0.3 BLUE=0.3

[gcode_macro LEDs_on_Red]
gcode:
    SET_LED LED=case_lights RED=0.5 GREEN=0 BLUE=0

[gcode_macro LEDs_on_Green]
gcode:
    SET_LED LED=case_lights RED=0 GREEN=0.3 BLUE=0

[gcode_macro LEDs_off]
gcode:
    SET_LED LED=case_lights RED=0 GREEN=0 BLUE=0

#####################################################################
#	Misc
#####################################################################

[gcode_macro RESETSPEEDS]
gcode:
    SET_VELOCITY_LIMIT VELOCITY={printer.configfile.settings.printer.max_velocity}
    SET_VELOCITY_LIMIT ACCEL={printer.configfile.settings.printer.max_accel}
    SET_VELOCITY_LIMIT ACCEL_TO_DECEL={printer.configfile.settings.printer.max_accel_to_decel}
    SET_VELOCITY_LIMIT SQUARE_CORNER_VELOCITY={printer.configfile.settings.printer.square_corner_velocity}

[gcode_macro OFF]
gcode:
    M84                                  ; turn steppers off
    TURN_OFF_HEATERS                     ; turn bed / hotend off
    M107                                 ; turn print cooling fan off
    LEDs_off

[gcode_macro TEST_SPEED]
gcode:
	# Speed
	{% set speed  = params.SPEED|default(printer.configfile.settings.printer.max_velocity)|int %}
	# Iterations
	{% set iterations = params.ITERATIONS|default(5)|int %}
	# Acceleration
	{% set accel  = params.ACCEL|default(printer.configfile.settings.printer.max_accel)|int %}
	# Bounding inset for large pattern (helps prevent slamming the toolhead into the sides after small skips, and helps to account for machines with imperfectly set dimensions)
	{% set bound = params.BOUND|default(20)|int %}
	# Size for small pattern box
	{% set smallpatternsize = SMALLPATTERNSIZE|default(20)|int %}

	# Large pattern
		# Max positions, inset by BOUND
		{% set x_min = printer.toolhead.axis_minimum.x + bound %}
		{% set x_max = printer.toolhead.axis_maximum.x - bound %}
		{% set y_min = printer.toolhead.axis_minimum.y + bound %}
		{% set y_max = printer.toolhead.axis_maximum.y - bound %}

	# Small pattern at center
		# Find X/Y center point
		{% set x_center = (printer.toolhead.axis_minimum.x|float + printer.toolhead.axis_maximum.x|float ) / 2 %}
		{% set y_center = (printer.toolhead.axis_minimum.y|float + printer.toolhead.axis_maximum.y|float ) / 2 %}

		# Set small pattern box around center point
		{% set x_center_min = x_center - (smallpatternsize/2) %}
		{% set x_center_max = x_center + (smallpatternsize/2) %}
		{% set y_center_min = y_center - (smallpatternsize/2) %}
		{% set y_center_max = y_center + (smallpatternsize/2) %}

	# Save current gcode state (absolute/relative, etc)
	SAVE_GCODE_STATE NAME=TEST_SPEED

	# Output parameters to g-code terminal
	{ action_respond_info("TEST_SPEED: starting %d iterations at speed %d, accel %d" % (iterations, speed, accel)) }

	# Home and get position for comparison later:
		G28
		# QGL if not already QGLd (only if QGL section exists in config)
		{% if printer.configfile.settings.quad_gantry_level %}
			{% if printer.quad_gantry_level.applied == False %}
				QUAD_GANTRY_LEVEL
				G28 Z
			{% endif %}
		{% endif %}
		# Move 50mm away from max position and home again (to help with hall effect endstop accuracy - https://github.com/AndrewEllis93/Print-Tuning-Guide/issues/24)
		G90
		G1 X{printer.toolhead.axis_maximum.x-50} Y{printer.toolhead.axis_maximum.y-50} F{30*60}
		G28 X Y
		G0 X{printer.toolhead.axis_maximum.x-1} Y{printer.toolhead.axis_maximum.y-1} F{30*60}
		G4 P1000
		GET_POSITION

	# Go to starting position
	G0 X{x_min} Y{y_min} Z{bound + 10} F{speed*60}

	# Set new limits
	SET_VELOCITY_LIMIT VELOCITY={speed} ACCEL={accel} ACCEL_TO_DECEL={accel / 2}

	{% for i in range(iterations) %}
		# Large pattern
			# Diagonals
			G0 X{x_min} Y{y_min} F{speed*60}
			G0 X{x_max} Y{y_max} F{speed*60}
			G0 X{x_min} Y{y_min} F{speed*60}
			G0 X{x_max} Y{y_min} F{speed*60}
			G0 X{x_min} Y{y_max} F{speed*60}
			G0 X{x_max} Y{y_min} F{speed*60}

			# Box
			G0 X{x_min} Y{y_min} F{speed*60}
			G0 X{x_min} Y{y_max} F{speed*60}
			G0 X{x_max} Y{y_max} F{speed*60}
			G0 X{x_max} Y{y_min} F{speed*60}

		# Small pattern
			# Small diagonals
			G0 X{x_center_min} Y{y_center_min} F{speed*60}
			G0 X{x_center_max} Y{y_center_max} F{speed*60}
			G0 X{x_center_min} Y{y_center_min} F{speed*60}
			G0 X{x_center_max} Y{y_center_min} F{speed*60}
			G0 X{x_center_min} Y{y_center_max} F{speed*60}
			G0 X{x_center_max} Y{y_center_min} F{speed*60}

			# Small box
			G0 X{x_center_min} Y{y_center_min} F{speed*60}
			G0 X{x_center_min} Y{y_center_max} F{speed*60}
			G0 X{x_center_max} Y{y_center_max} F{speed*60}
			G0 X{x_center_max} Y{y_center_min} F{speed*60}
	{% endfor %}

	# Restore max speed/accel/accel_to_decel to their configured values
	SET_VELOCITY_LIMIT VELOCITY={printer.configfile.settings.printer.max_velocity} ACCEL={printer.configfile.settings.printer.max_accel} ACCEL_TO_DECEL={printer.configfile.settings.printer.max_accel_to_decel}

	# Re-home and get position again for comparison:
		G28 X Y
		# Go to XY home positions (in case your homing override leaves it elsewhere)
		G90
		G0 X{printer.toolhead.axis_maximum.x-1} Y{printer.toolhead.axis_maximum.y-1} F{30*60}
		G4 P1000
		GET_POSITION

	# Restore previous gcode state (absolute/relative, etc)
	RESTORE_GCODE_STATE NAME=TEST_SPEED

[delayed_gcode DELAYED_OFF]
gcode:
    OFF ; call "OFF" macro, to turn off everything (heaters, motors, lights, fans)
