# 3D Printer Configs


## What this is:

This is my personal collection of printer firmwares / configs
and related profiles for [SuperSlicer](https://github.com/supermerill/SuperSlicer)

You are free to use, copy, cange and redistribute them to your hearts content.

## What this is not:

Since the configs and profiles are highly customized, these are in no way
meant to work on other printers.

Use / Change them only if you now what you're doing.
